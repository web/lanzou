# lanzou

#### 介绍
蓝奏云下载地址解析API

https://gitee.com/web/lanzou

其它版本api  https://gitee.com/web/lianyi-lzapi

#### 软件架构
1.支持检测文件是否被取消

2.支持带密码的文件分享链接但不支持分享的文件夹

3.支持生成直链或直接下载

4.支持ios应用在线安装获取地址


#### 更新说明
1.新版本需要自己部署到自己服务器写上自己的参数
2.打开lanzou.clsss.php 修改里面的base_domain 和 lz_cookie参数
base_domain 可通过分享文件获取分享链接

lz_cookie 获取cookie(浏览器F12控制台执行)：
```javascript
	if(!/(^|\.)woozooo\.com$/i.test(document.location.host))
		throw new Error('请登录到蓝奏云控制台在执行此代码！');
	
	var regex = /(?<=^|;)\s*([^=]+)=\s*(.+?)\s*(?=;|$)/g,
		cookies = {},re;
	while(re = regex.exec(document.cookie))
		if(re[1] === 'ylogin'||re[1] === 'phpdisk_info')
			cookies[re[1]] = re[1]+'='+re[2]+';';
	
	if(!cookies.hasOwnProperty('phpdisk_info'))
		throw new Error('获取cookie失败，请确认您已登录到蓝奏云控制台！');
	
	(function (str) {
		var oInput = document.createElement('input');
		oInput.value = str;
		document.body.appendChild(oInput);
		oInput.select();
		document.execCommand("Copy");
		oInput.remove();
		alert('复制成功');
	})(Object.values(cookies).join(' '));
```

#### 使用说明

url:蓝奏云外链链接

type:是否直接下载 值：down

pwd:外链密码

内部调用方法
```php
include('lanzou.clsss.php');
$lz = new lanzou;
$res=$lz->getUrl($url,$pwd);
```

直接下载：
无密码：http://tool.bitefu.net/lanzou/?url=https://www.lanzous.com/xxxx&type=down

有密码：http://tool.bitefu.net/lanzou/?url=https://www.lanzous.com/xxxxx&type=down&pwd=1234

输出直链：
无密码：http://tool.bitefu.net/lanzou/?url=https://www.lanzous.com/xxxxx

有密码：http://tool.bitefu.net/lanzou/?url=https://www.lanzous.com/xxxx&pwd=1234

简网址

不带密码:http://tool.bitefu.net/lanzou/?d=iXAYR0e10dpe

带密码:http://tool.bitefu.net/lanzou/?d=ic3qfri-52pj

#### 参与贡献
参考开源项目:https://github.com/MHanL/LanzouAPI
